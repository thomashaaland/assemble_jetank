# Assembling the JETANK

## Assembly

The Jetank was already assembled when I got it, so I had to disassemble it first. Everything seeemed good, though the claw was a bit loose. 

![](20221007_110631.jpg)

After removing the claw, top chassis, belts and the WiFi board, the tank was almost ready to be assembled once again.  

![](20221007_114941.jpg)

Removing the JETSON board revealed the batterier underneath and after a quick inspection everything seemed in order.

![](20221007_115619.jpg)

The stripped down JETSON ready to be assembled.

![](20221007_120027.jpg)

While fastening the suppport elements to the JETSON board one of the elements came off of the board. However, I still managed to fasten it well enough and despite the mishap the JETSON didn't seem affected.

![](20221009_134453.jpg)

The JETSON board and WiFi board reassembled.

![](20221007_123902.jpg)

Now the only things left to do were to fasten the top chassis. This presented some problems due to the loose support element, though after some fiddling it was done.

![](20221007_154041.jpg)

The final JETSON bot after assembly and testing. It worked despite some trouble and all servos seems to be in order so far. 

![](20221009_145935.jpg)

The most trouble came with Etcher and downloading the software. I had to do both at home and come back the next day since company software didn't allow for either. However, once the SD card was etched with Etcher setting up the software on the robot was easy. 
